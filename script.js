



// Activity
// 3. Create an addStudent() function that will accept a name of the student and add it to the student array.
let list1 = [];

function addStudent(student){
	console.log(student + ` has been added to the list`)
	return list1.push(student)
};


// 4. Create a countStudents() function that will print the total number of students in the array.
function countStudents(){
	return (console.log(`There are a total of `+list1.length+` students enrolled`));
	
};

// 5. Create a printStudents() function that will sort and individually print the students (sort and forEach methods) in the array.

function printStudents(){
	list1.sort();
	console.log(list1);
}
list1.forEach(printStudents);

// 6. Create a findStudent() function that will do the following: 
	
	//a. Search for a student name when a keyword is given (filter method).

	//b. If one match is found print the message studentName is an enrollee.

	//c. If multiple matches are found print the message studentNames are enrollees.

	//d. If no match is found print the message studentName is not an enrollee.

	//e. The keyword given should not be case sensitive.

// 7. Create a git repository named s17.

// 8. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code

// 9. Add the link in Boodle.

// Stretch Goals
// 1. Create an addSection() function that will add a section to all students in the array with the format of studentName - Section A (map method).

// 2. Create a removeStudent() Function that will do the following:

	// a. Capitalize the first letter of the user's input (toUpperCase and slice methods).
	// b. Retrieve the index of the student to be removed (indexOf method).
	// c. Remove the student from the array (splice method).
	// d. Print a message that the studentName was removed from the student's list.
















































































